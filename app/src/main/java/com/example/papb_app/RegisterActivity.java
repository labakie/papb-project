package com.example.papb_app;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    EditText editTextRegisterName, editTextRegisterEmail, editTextRegisterPassword;
    Button buttonRegister;
    TextView textViewLogin;
    FirebaseAuth mAuth;
    FirebaseFirestore db;
    ProgressDialog progressDialog;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editTextRegisterName = findViewById(R.id.editTextRegisterName);
        editTextRegisterEmail = findViewById(R.id.editTextRegisterEmail);
        editTextRegisterPassword = findViewById(R.id.editTextRegisterPass);
        buttonRegister = findViewById(R.id.buttonRegister);
        textViewLogin = findViewById(R.id.textViewLogin);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");

        buttonRegister.setOnClickListener(view -> {
            createUser();
        });

        textViewLogin.setOnClickListener(view -> {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        });
    }

    private void createUser() {
        progressDialog.show();
        String name = editTextRegisterName.getText().toString();
        String email = editTextRegisterEmail.getText().toString();
        String password = editTextRegisterPassword.getText().toString();

        if (TextUtils.isEmpty(name)) {
            progressDialog.dismiss();
            editTextRegisterName.setError("Name can't be empty");
            editTextRegisterName.requestFocus();
        } else if (TextUtils.isEmpty(email)) {
            progressDialog.dismiss();
            editTextRegisterEmail.setError("Email can't be empty");
            editTextRegisterEmail.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            progressDialog.dismiss();
            editTextRegisterEmail.setError("Password can't be empty");
            editTextRegisterPassword.requestFocus();
        } else {
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener((task) -> {
                if (task.isSuccessful()) {
                    Toast.makeText(RegisterActivity.this, "Registration success", Toast.LENGTH_SHORT).show();
                    userID = mAuth.getCurrentUser().getUid();
                    DocumentReference documentReference = db.collection("users").document(userID);
                    Map<String, Object> user = new HashMap<>();
                    user.put("name", name);
                    user.put("email", email);
                    documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Log.d(TAG, "onSuccess: admin profile is created for " + userID);
                        }
                    });
                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(RegisterActivity.this, "Registration Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}