# PAPB Project - Item Management App

## Description
The main function of this app is item management, allowing each user to store item information such as the image, name, and price. This app was made using Java on Android Studio and integrated with Google Firebase to help the app function run easier, namely:
* Firebase authentication for managing user login and registration.
* The NoSQL database Firebase Cloud Firestore for storing and integrating user and item data.
* Firebase storage for store item images.
* Basic CRUD operations and the RecyclerView.